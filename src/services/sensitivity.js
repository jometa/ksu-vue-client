import API from './axiosInstance'

export default {
  post (prio) {
    return API.post('/api/sensitivity', { prio }).then(resp => resp.data)
  }
}
